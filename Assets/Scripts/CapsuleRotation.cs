using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    [SerializeField]

    private Vector3 direction;
    public float speed;

    void update ()
    {
        direction = ClampVector3(direction);
        transform.Rotate(direction * (speed * Time.deltaTime));

    }

    public static Vector3 ClampVector3(Vector3 target)
    {

        float clampedx = Mathf.Clamp(target.x, -1f, 1f);
        float clampedy = Mathf.Clamp(target.y, -1f, 1f);
        float clampedz = Mathf.Clamp(target.z, -1f, 1f);

        Vector3 result = new Vector3(clampedx, clampedy, clampedz);

        return result;
    }
}
