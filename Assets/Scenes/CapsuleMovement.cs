using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour
{
    [SerializeField]
    private Vector3 direction; //Posibles ejes de movimiento
    public float speed; //Velocidad de movimiento

    void update()
    {
        direction = ClampVector3(direction);

        transform.Translate(direction * (speed * Time.deltaTime));

    }


    public static Vector3 ClampVector3(Vector3 target)
    {
        float clampedx = Mathf.Clamp(target.x, -1f, 1f);
        float clampedy = Mathf.Clamp(target.y, -1f, 1f);
        float clampedz = Mathf.Clamp(target.z, -1f, 1f);

        Vector3 result = new Vector3(clampedx, clampedy, clampedz);

        return result;
    }

}
